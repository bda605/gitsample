﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace gitsample
{
    class Program
    {
        static void Main(string[] args)
        {
        
            List<int> listInt = new List<int>() { 1, 2, 3, 4, 5 };

            Parallel.ForEach(listInt, q =>
            {
                Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"));
            });
            Console.ReadKey();


            Console.WriteLine("測試版號");
            Console.WriteLine("退版");
            Console.WriteLine("退版");
        }
        /// <summary>
        /// 測試註解
        /// </summary>
        private static void NewMethod()
        {
            for (int i = 0; i < 2; i++)
            {
                Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"));
            }
        }
    }
}
